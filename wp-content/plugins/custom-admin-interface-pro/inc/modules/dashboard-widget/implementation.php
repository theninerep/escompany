<?php

    /**
    * 
    *
    *
    * Dashboard content
    */
    function dashboard_widget_output( $var, $args ) {
        echo custom_admin_interface_pro_shortcode_replacer($args['args']); 
    }

    /**
    * 
    *
    *
    * Function to add new dashboard widget
    */
    add_action( 'wp_dashboard_setup', 'dashboard_widget_implementation' );
    function dashboard_widget_implementation() {

        //we need to get all published posts and loop through them
        $args = array(
            'post_type'      => 'dashboard_widget',
            'post_status'    => 'publish',
            'posts_per_page' => -1
        );

        $posts = get_posts( $args );

        if($posts){
            foreach($posts as $post){

                $post_id = $post->ID;
                
                //check if the code needs to be executed
                if(custom_admin_interface_pro_exception_check($post_id)){

                    $widget_title = get_post_meta($post_id, 'widget_title', true);
                    $widget_content = get_post_meta($post_id, 'widget_content', true);
    
                    if(strlen($widget_title) < 1) {
                        $widget_title = "Custom Widget";      
                    }
                    
                    wp_add_dashboard_widget(
                        'dashboard_widget_'.$post_id, //slug
                        $widget_title, //title
                        'dashboard_widget_output', //function
                        null,
                        $widget_content
                    );

                    
            
                } //end exception check
            } //end foreach post
        } //end post check

    }
    
?>