<?php
    /**
    * 
    *
    *
    * Implement custom toolbar
    */
    add_action( 'admin_bar_menu', 'admin_toolbar_implementation', PHP_INT_MAX );
    function admin_toolbar_implementation( $wp_admin_bar ) {
        
        //get original toolbar items
        $original_toolbar_nodes = $wp_admin_bar->get_nodes();

        //create a global variable
        global $default_wordpress_toolbar;
        //set the variable to all nodes
        $default_wordpress_toolbar = $original_toolbar_nodes;


        //lets implement the menu
        //we are going to follow a similar procedure to how our other posts are implemented
        //we need to get all published posts and loop through them
        $args = array(
            'post_type'      => 'admin_toolbar',
            'post_status'    => 'publish',
            'posts_per_page' => -1
        );

        $posts = get_posts( $args );

        if($posts){
            foreach($posts as $post){

                $post_id = $post->ID;
                
                $disable_on_frontend = get_post_meta($post_id, 'disable_frontend', true);

                if( (is_admin() && $disable_on_frontend == 'checked') || $disable_on_frontend != 'checked' ){

                    //check if the code needs to be executed
                    if(custom_admin_interface_pro_exception_check($post_id)){

                        //get users to hide
                        $custom_toolbar = get_post_meta($post_id, 'custom_toolbar', true);

                        //lets do a check to make sure there's json
                        if(strpos($custom_toolbar, '{') !== false){
                            $custom_toolbar_array = json_decode($custom_toolbar);    

                            //create an array which will store all our notifications
                            $items_with_spans = array();
                            $items_meta = array();

                            //cycle through each node in the toolbar
                            foreach($original_toolbar_nodes as $item) {
                                //item id
                                $item_id = $item->id;
                                //item meta
                                $item_meta = $item->meta;
                                //lets add the id and meta to our array
                                $items_meta += array($item_id => $item_meta);
                                //item title
                                $item_title = $item->title;
                                //if item title contains a span lets add it to the array
                                if(strpos($item_title,'<span') !== false) {
                                    $items_with_spans += array($item_id => $item_title);    
                                }
                            }

                            //now we need to add this information to our settings
                            foreach($custom_toolbar_array as $item) {
                                //declare the id as a variable    
                                $item_id = $item->id;
                                //lets replace the meta
                                if(array_key_exists($item_id,$items_meta)){
                                    $meta_to_utilise = $items_meta[$item_id];
                                    $item->meta = $meta_to_utilise; 
                                } 
                                
                                //lets replace the titles
                                if(array_key_exists($item_id,$items_with_spans)){
                                    $title_to_utilise = $items_with_spans[$item_id];
                                    $item->title = $title_to_utilise;
                                }
                            }

                            // because wordpress wont just let us replace the global variable like it does on the main menu we need to remove all nodes from the original and then add our new nodes
                            // so lets first remove all the nodes
                            foreach($original_toolbar_nodes as $key => $val) {
                                $current_node = $original_toolbar_nodes[$key];
                                $wp_admin_bar->remove_node($key);
                            }

                            // var_dump($custom_toolbar_array);

                            //now lets add our nodes...thanks wordpress!
                            foreach($custom_toolbar_array as $item) {
                            
                                $args = array(
                                    'id'    => $item->id,
                                    'title' => $item->title,
                                    'parent' => $item->parent,
                                    'href'  => $item->href,
                                    'group' => $item->group,
                                    // 'meta'  => $item->meta
                                );

                                if(property_exists($item,'meta')){
                                    $args['meta'] = $item->meta;
                                }

                                //if the item is a logout link, let's change the URL
                                if($item->id == 'logout'){
                                    $args['href'] = wp_login_url().'?action=logout&_wpnonce='. wp_create_nonce( 'log-out' );
                                }
                                

                                $wp_admin_bar->add_node( $args );     
                            }



                            //now lets hide items from the toolbar
                            $hide_items = get_post_meta($post_id, 'toolbar_items_to_remove', true);
                            $hide_items_array = explode(',', $hide_items);

                            foreach ($hide_items_array as $value) {
                                $wp_admin_bar->remove_node($value);  
                            }
                            


                        }
                    }

                } //end exception check
            } //end foreach post
        } //end post check



    }
    

?>