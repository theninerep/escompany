=== Custom Admin Interface Pro ===
Contributors: northernbeacheswebsites
Donate link: https://northernbeacheswebsites.com.au/product/donate-to-northern-beaches-websites/
Tags: admin, interface, menu, menu editor, customize, toolbar, login, admin logo, admin login logo, change login logo, custom admin logo, custom code, maintenance, color scheme, admin notice, hide plugins, hide users, hide sidebars, hide meta, hide meta boxes
Requires at least: 3.0.1
Tested up to: 5.4.2
Stable tag: 1.28
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Custom Admin Interface Pro

== Description ==

Custom Admin Interface Pro

== Installation ==

There are a couple of methods for installing and setting up this plugin.

= Upload Manually =

1. Download and unzip the plugin
2. Upload the 'custom-admin-interface-pro' folder into the '/wp-content/plugins/' directory
3. Go to the Plugins admin page and activate the plugin

== Frequently Asked Questions ==


== Screenshots ==


== Changelog ==

= 1.28 =
* Minor bug fixes

= 1.27 =
* Small bug fix from 1.26 update

= 1.26 =
* We now add newly added sub menu items from plugins to the menu as hidden items

= 1.25 =
* Menu editor bug fixes

= 1.24 =
* Small update to FAQ

= 1.23 =
* Bug fix relating to file type of file

= 1.22 =
* Improved compatibility with WooCommerce and main menu
* New walkthrough video on help page

= 1.21 =
* Further improvements to logout link

= 1.20 =
* Fixing of noonce issue caused by custom toolbar logout link

= 1.19 =
* Compatibility fix with 3rd party plugins with custom menu

= 1.18 =
* Bug fix with targeting new toolbar items with css

= 1.17 =
* Buf fix for custom admin jquery not working as expected

= 1.16 =
* Changing of global variable to increase compatibility with other themes and plugins

= 1.15 =
* Fix for update message

= 1.14 =
* Fix for login url

= 1.13 =
* Fix for import issue

= 1.12 =
* Better treatment of quotes in plugin settings
* Testing with WordPress 5.2.4

= 1.11 =
* Critical update to improve import/export routine to improve reliability. This update will break existing exports that you may have created. SORRY FOR THIS!!! However this update makes import/exports far far more reliable. So please create a new export and then use this.

= 1.10 =
* Now on the exception list you can type filter items in the dropdown to make it easier to find specific roles/users you want to include/exclude

= 1.9 =
* Now have a fallback to display the users nickname in the conditional list

= 1.8 =
* Smarter ordering of code

= 1.7 =
* Better compatibility with PHP 7.3

= 1.6 =
* Multiple bug fixes

= 1.5 =
* Bug fixes for main menu editing to improve compatibility with 3rd party plugins. Now plugins which have HTML in the menu title we don't allow to be edited and we just serve the original. Although this means less flexibility it does mean it will prevent things from breaking which is more important

= 1.4 =
* Added the ability to duplicate posts, pages and custom post types from the general settings, probably saving you another plugin. It also means you can duplicate menu and toolbar settings!

= 1.3 =
* Added the ability to add a background image, size, position and repeat to both the custom maintenance and login pages

= 1.2 =
* Bug fixes

= 1.1 =
* Added an FAQ for the plugin

= 1.0 =
* Initial launch of the plugin



== Upgrade Notice ==

= 1.28 =
* Minor bug fixes

= 1.27 =
* Small bug fix from 1.26 update

= 1.26 =
* We now add newly added sub menu items from plugins to the menu as hidden items

= 1.25 =
* Menu editor bug fixes

= 1.24 =
* Small update to FAQ

= 1.23 =
* Bug fix relating to file type of file

= 1.22 =
* Improved compatibility with WooCommerce and main menu
* New walkthrough video on help page

= 1.21 =
* Further improvements to logout link

= 1.20 =
* Fixing of noonce issue caused by custom toolbar logout link

= 1.19 =
* Compatibility fix with 3rd party plugins with custom menu

= 1.18 =
* Bug fix with targeting new toolbar items with css

= 1.17 =
* Buf fix for custom admin jquery not working as expected

= 1.16 =
* Changing of global variable to increase compatibility with other themes and plugins

= 1.15 =
* Fix for update message

= 1.14 =
* Fix for login url

= 1.13 =
* Fix for import issue

= 1.12 =
* Better treatment of quotes in plugin settings
* Testing with WordPress 5.2.4

= 1.11 =
* Critical update to improve import/export routine to improve reliability. This update will break existing exports that you may have created. SORRY FOR THIS!!! However this update makes import/exports far far more reliable. So please create a new export and then use this.

= 1.10 =
* Now on the exception list you can type filter items in the dropdown to make it easier to find specific roles/users you want to include/exclude

= 1.9 =
* Now have a fallback to display the users nickname in the conditional list

= 1.8 =
* Smarter ordering of code

= 1.7 =
* Better compatibility with PHP 7.3

= 1.6 =
* Multiple bug fixes

= 1.5 =
* Bug fixes for main menu editing to improve compatibility with 3rd party plugins. Now plugins which have HTML in the menu title we don't allow to be edited and we just serve the original. Although this means less flexibility it does mean it will prevent things from breaking which is more important

= 1.4 =
* Added the ability to duplicate posts, pages and custom post types from the general settings, probably saving you another plugin. It also means you can duplicate menu and toolbar settings!

= 1.3 =
* Added the ability to add a background image, size, position and repeat to both the custom maintenance and login pages

= 1.2 =
* Bug fixes

= 1.1 =
* Added an FAQ for the plugin

= 1.0 =
* This is the first version of the plugin.