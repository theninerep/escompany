<?php get_template_part('template-parts/portfolio/portfolio-title', flatsome_option('portfolio_title')); ?>

<div class="portfolio-top">
<section class="section tn-podet-intro-tit">
    <div class="bg section-bg fill bg-fill bg-loaded bg-loaded"></div>
    <div class="section-content relative">
        <div class="row" id="row-198248562">
            <div id="col-1353223405" class="col small-12 large-12">
                <div class="col-inner">
                    <div id="text-3503946243" class="text">
                        <div class="tn-tit-box no-shadow">
                            <h2 class="tn-spec-tit">Giới thiệu</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="section tn-podet-intro">
    <div class="bg section-bg fill bg-fill bg-loaded bg-loaded"></div>
    <div class="section-content relative">
        <div class="row row-large" id="row-1974514838">
            <div id="col-1469654560" class="col small-12 large-12">
                <div class="col-inner">
                  <?php get_template_part('template-parts/portfolio/portfolio-content'); ?>
                </div>
            </div>
            <!--
            <div id="col-657100950" class="col medium-6 small-12 large-6">
                <div class="col-inner">
                      <ul>
                        <li class="tn-portfolio-square">
                          <?php
                          $tong_quan = get_field('tong_quan');
                          if( $tong_quan ): ?>
                           <p><?php echo $tong_quan['dien_tich'] ?></p>
                          <?php endif; ?>
                        </li>
                        <li class="tn-portfolio-kind">
                          <?php
                          $tong_quan = get_field('tong_quan');
                          if( $tong_quan ): ?>
                           <p><?php echo $tong_quan['loai_hinh'] ?></p>
                          <?php endif; ?>
                        </li>
                        <li class="tn-portfolio-kind">
                          <?php
                          $tong_quan = get_field('tong_quan');
                          if( $tong_quan ): ?>
                           <p><?php echo $tong_quan['tinh_trang'] ?></p>
                          <?php endif; ?>
                        </li>
                        <li class="tn-portfolio-kind">
                          <?php
                          $tong_quan = get_field('tong_quan');
                          if( $tong_quan ): ?>
                           <p><?php echo $tong_quan['san_pham'] ?></p>
                          <?php endif; ?>
                        </li>
    						      </ul>
                </div>
                
            </div>-->
        </div>
    </div>
</section>
<section class="section tn-podet-location-tit">
    <div class="bg section-bg fill bg-fill bg-loaded bg-loaded"></div>
    <div class="section-content relative">
        <div class="row" id="row-1398166765">
            <div id="col-2095180115" class="col small-12 large-12">
                <div class="col-inner">
                    <div id="text-2262022498" class="text">
                        <div class="tn-tit-box no-shadow">
                            <h2 class="tn-spec-tit">Vị trí</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="section tn-podet-location">
    <div class="bg section-bg fill bg-fill bg-loaded bg-loaded"></div>
    <div class="section-content relative">
        <div class="row row-large" id="row-2071131204">
            <div id="col-363962797" class="col small-12 large-12">
                <div class="col-inner">
                    <?php echo the_field('vi_tri'); ?>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="section tn-podet-widget-tit">
    <div class="bg section-bg fill bg-fill bg-loaded bg-loaded"></div>
    <div class="section-content relative">
        <div class="row" id="row-162775480">
            <div id="col-703323089" class="col small-12 large-12">
                <div class="col-inner">
                    <div id="text-3016627775" class="text">
                        <div class="tn-tit-box no-shadow">
                            <h2 class="tn-spec-tit">Tiện ích</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="section tn-podet-media">
    <div class="bg section-bg fill bg-fill bg-loaded bg-loaded"></div>
    <div class="section-content relative">
        <div class="row row-large" id="row-1685868515">
            <div id="col-1413024443" class="col small-12 large-12">
                <div class="col-inner">
                  <?php 
                  $images = get_field('tien_ich');
                  if( $images ): ?>
                      <ul>
                          <?php foreach( $images as $image ): ?>
                              <li>
                                  <a rel="image_group" href="<?php echo esc_url($image['url']); ?>" data-lightbox="<?php echo esc_html($image['caption']); ?>">
                                       <img rel="image_group" class="fancybox" src="<?php echo esc_url($image['sizes']['large']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
                                  </a>
                              </li>
                          <?php endforeach; ?>
                      </ul>
                  <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="section tn-podet-floor-tit">
    <div class="bg section-bg fill bg-fill bg-loaded bg-loaded"></div>
    <div class="section-content relative">
        <div class="row" id="row-1296414945">
            <div id="col-1763410363" class="col small-12 large-12">
                <div class="col-inner">
                    <div id="text-308578676" class="text">
                        <div class="tn-tit-box no-shadow">
                            <h2 class="tn-spec-tit">Mặt bằng</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="section tn-podet-media tn-podet-floor">
    <div class="bg section-bg fill bg-fill bg-loaded bg-loaded"></div>
    <div class="section-content relative">
        <div class="row row-large" id="row-1841631030">
            <div id="col-1190303105" class="col small-12 large-12">
                <div class="col-inner">
                  <?php 
                  $images = get_field('mat_bang');
                  if( $images ): ?>
                      <ul>
                          <?php foreach( $images as $image ): ?>
                              <li>
                                  <a rel="image_group" href="<?php echo esc_url($image['url']); ?>" data-lightbox="<?php echo esc_html($image['caption']); ?>">
                                       <img rel="image_group" class="fancybox" src="<?php echo esc_url($image['sizes']['large']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
                                  </a>
                              </li>
                          <?php endforeach; ?>
                      </ul>
                  <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="section tn-podet-media-tit">
    <div class="bg section-bg fill bg-fill bg-loaded bg-loaded"></div>
    <div class="section-content relative">
        <div class="row" id="row-2123179889">
            <div id="col-1461410700" class="col small-12 large-12">
                <div class="col-inner">
                    <div id="text-3726761558" class="text">
                        <div class="tn-tit-box no-shadow">
                            <h2 class="tn-spec-tit">Thư viện</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="section tn-podet-media">
    <div class="bg section-bg fill bg-fill bg-loaded bg-loaded"></div>
    <div class="section-content relative">
        <div class="row row-large" id="row-1454081068">
            <div id="col-134122464" class="col small-12 large-12">
                <div class="col-inner">
                <?php //$images = get_field('thu_vien'); echo do_shortcode('[ux_gallery ids="$images" style="default" lightbox_image_size="large" type="slider" slider_nav_style="simple" slider_nav_color="light" slider_nav_position="outside" image_height="168px" image_size="original" text_align="center" class="tn-portfolio-slider"]'); ?>
                <?php 
                  $images = get_field('thu_vien');
                  if( $images ): ?>
                      <ul>
                          <?php foreach( $images as $image ): ?>
                              <li>
                                  <a rel="image_group" href="<?php echo esc_url($image['url']); ?>" data-lightbox="<?php echo esc_html($image['caption']); ?>">
                                       <img rel="image_group" class="fancybox" src="<?php echo esc_url($image['sizes']['large']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
                                  </a>
                              </li>
                          <?php endforeach; ?>
                      </ul>
                  <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>


</div>

<div class="portfolio-bottom">
	<?php get_template_part('template-parts/portfolio/portfolio-next-prev'); ?>
	<?php get_template_part('template-parts/portfolio/portfolio-related'); ?>
</div>